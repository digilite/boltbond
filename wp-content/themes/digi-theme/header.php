<?php 
$email = get_field('email', 'option');
$phone = get_field('phone_number', 'option');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header itemscope itemtype="http://schema.org/WPHeader">
		<div class="container-fluid flex-container header-items">
			<div itemscope itemtype="http://schema.org/Organization" id="logo">
				<a itemprop="url" href="<?php echo bloginfo('url') ?>">
					<img class="logo" itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo_b.svg">
				</a>
			</div>
			<button type="button" data-toggle="modal" class="menu-toggle relative-parent">
            	<div id="burger-icon" class="">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</button>
		</div>
		<div class="modal-menu">
			<p class="menu-title text-center">MENU</p>
			<nav itemscope itemtype="http://schema.org/SiteNavigationElement"><?php
				wp_nav_menu([
					'theme_location' => 'primary-menu',
					'menu_class' => 'main-menu',
					'container' => '',
				]); ?>
			</nav>
			<?php 
			$clear_phone = str_replace(' ', '', $phone);
			$part_phone = str_replace(')', '', $clear_phone);
			$part2_phone = str_replace('(', '', $part_phone);
			$final_phone = str_replace('-', '', $part2_phone);
			?>
			<p class="contact-info"><a href="mailto:<?= $email; ?>" class="email"><?= $email; ?></a> | <a href="tel:<?= $final_phone; ?>"><?= $phone; ?></a></p>
			<div class="menu-social">
				<?php get_template_part("social"); ?>
			</div>
		</div>
	</header>