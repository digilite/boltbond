<?php 
/**
* Template Name: Homepage
*
* @package WordPress
* @subpackage Digilite Theme
*/
get_header(); 
get_template_part("templates/hero");
get_template_part("content");
get_footer(); ?>