<?php 
$email = get_field('email', 'option');
$phone = get_field('phone_number', 'option');
$ft_back = get_field('footer_background', 'option');
?>
		<footer itemscope itemtype="http://schema.org/WPFooter" style="background: url('<?= $ft_back; ?>') right center / cover no-repeat;">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="footer-left">
							<?php 
								$clear_phone = str_replace(' ', '', $phone);
								$part_phone = str_replace(')', '', $clear_phone);
								$part2_phone = str_replace('(', '', $part_phone);
								$final_phone = str_replace('-', '', $part2_phone);
							?>
							<a itemprop="url" href="<?php echo bloginfo('url') ?>">
								<img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo_small.svg"></a>
								<a href="mailto:<?= $email; ?>" class="email"><?= $email; ?></a>
								<a href="tel:<?= $final_phone; ?>" class="phone"><?= $phone; ?></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="footer-right flex-container">
							<div class="footer-nav">
								<?php wp_nav_menu([
								'theme_location' => 'primary-menu',
								'menu_class' => 'main-menu flex-container',
								'container' => '',
								]); ?>
							</div>
						</div>
						<div class="footer-social flex-container justify-content-end">
		                	<?php get_template_part("social"); ?>
		           		</div>
					</div>
				</div>
				<p class="copy">Bolt And Bond. All Rights Reserved <?php echo date("Y") ?></p>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>