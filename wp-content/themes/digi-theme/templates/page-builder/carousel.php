<?php 
$services_title = get_sub_field('scar_title');
$services_button = get_sub_field('scar_button_label');
$services_button_url = get_sub_field('scar_button_url');
?>
<section class="services">
    <h2 class="block_title text-center"><?= $services_title; ?></h2>
    <div id="services" class="owl-theme owl-carousel services-list">
        <?php 
        $args = array( 
            'post_type' => 'services',
            'posts_per_page' => -1
        ); 

        $loop = new WP_Query( $args ); 
       
        ?>
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="item service-item">
            <?php echo get_the_post_thumbnail( get_the_ID(), 'full'); ?>
            <h4 class="service-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <a href="<?= $services_button_url; ?>" class="button cta-button cta-button-grey"><span>+</span><?= $services_button; ?></a>
</section>