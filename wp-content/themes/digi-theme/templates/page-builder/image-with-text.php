<?php
$img_txt_first_image = get_sub_field('img_txt_first_image');
$img_txt_second_image = get_sub_field('img_txt_second_image');
$img_txt_title = get_sub_field('img_txt_title');
$img_txt_description = get_sub_field('img_txt_description');
$img_txt_cta_text = get_sub_field('imgtx_cta_text');
$img_txt_button_label = get_sub_field('img_txt_button_label');
$img_txt_button_url = get_sub_field('img_txt_button_link');
$img_txt_size = 'full';
$block_style = get_sub_field('choose_style_img_txt');
?>
<?php if($img_txt_title && $block_style != 'img_left'): ?>
    <h2 class="block_title mobile-block-title"><?= $img_txt_title; ?></h2>
<?php endif; ?>
<section class="img-with-text-section <?= $block_style; ?>">
    <div class="img-with-text">
        <div class="container-fluid">
            <div class="row <?php if($block_style == 'img_left') : echo 'justify-content-center'; else: echo 'justify-content-start image-right-style'; endif; ?>  items-center">
                <div class="<?php if($block_style == 'img_left') : echo 'col-md-6'; else: echo 'col-md-5'; endif; ?>">
                    <div class="img_txt_images">
                        <?php
                        if($block_style == 'img_left') :
                        if($img_txt_first_image): 
                        echo wp_get_attachment_image( $img_txt_first_image, $img_txt_size );
                        endif;
                        if($img_txt_second_image):
                        echo wp_get_attachment_image( $img_txt_second_image, $img_txt_size );
                        endif; 
                        else :
                        if($img_txt_title): ?>
                        <h2 class="block_title"><?= $img_txt_title; ?></h2>
                        <?php endif; 
                        if($img_txt_description):?>
                        <p class="description"><?= $img_txt_description; ?></p>
                        <?php endif;
                        if($img_txt_cta_text) : ?>
                        <h4 class="img-cta-text"><?= $img_txt_cta_text; ?></h4>
                        <?php endif;
                        if($img_txt_button_label) : ?>
                        <a href="<?= $img_txt_button_url; ?>" class="button cta-button"><span>+</span><?= $img_txt_button_label; ?></a>
                        <?php endif;
                        endif;  ?>
                    </div>
                </div>
                <div class="col-md-5">
                    <?php
                    if($block_style == 'img_left') :
                    if($img_txt_title): ?>
                    <h2 class="block_title"><?= $img_txt_title; ?></h2>
                    <?php endif; 
                    if($img_txt_description):?>
                    <p class="description"><?= $img_txt_description; ?></p>
                    <?php endif;
                    if($img_txt_cta_text) : ?>
                    <h4 class="img-cta-text"><?= $img_txt_cta_text; ?></h4>
                    <?php endif;
                    if($img_txt_button_label) : ?>
                    <a href="<?= $img_txt_button_url; ?>" class="button cta-button"><span>+</span><?= $img_txt_button_label; ?></a>
                    <?php endif;
                    else:
                    if($img_txt_first_image): 
                        echo wp_get_attachment_image( $img_txt_first_image, $img_txt_size );
                    endif;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>