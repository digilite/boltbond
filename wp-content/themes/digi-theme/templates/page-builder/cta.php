<?php
$cta_title = get_sub_field('cta_title');
$cta_title_size = get_sub_field('cta_title_size');
$cta_title_style = get_sub_field('cta_title_style');
$cta_button = get_sub_field('cta_button_label');
$cta_button_url = get_sub_field('cta_button_url');
?>
<section class="cta">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <h2 class="cta-title" style="font-size: <?= $cta_title_size;?>; text-transform: <?= $cta_title_style; ?>"><?= $cta_title; ?></h2>
                <a href="<?= $cta_button_url; ?>" class="button cta-button"><span>+</span><?= $cta_button; ?></a>
            </div>
        </div>
    </div>
</section>