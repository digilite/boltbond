<?php
$title = get_sub_field('title');
$hashtag = get_sub_field('hashtag');
$button = get_sub_field('button');
?>
<section class="cta">
    <div class="container-fluid">
        <h3 class="insta-title text-center"><?= $title; ?></h3>
        <p class="hashtag text-center"><?= $hashtag; ?></p>
        <?php echo do_shortcode('[instagram-feed feed=1]'); ?>
        <a href="<?= $button['url']; ?>" class="button cta-button"><span>+</span><?= $button['title']; ?></a>
    </div>
</section>