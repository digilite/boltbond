<?php
$total_rows = count(get_sub_field('gallery_r')); # всего изображений 
$count = 0;  # счётчик 
$number = 2; # сколько изображений отображать на каждой странице 
?>


<?php
// Если есть вложенные поля (изображения) 
if (have_rows('gallery_r')) { ?>

    <div class="ajax-container">

        <?php
        while (have_rows('gallery_r')) {
            the_row();

            if ($count == $number) {
                break; # если показали все изображения, выходим из цикла 
            } ?>

            <?php $img_object = get_sub_field('image_r'); ?>

            <a data-fancybox="gallery" href="<?php echo esc_url($img_object['sizes']['large']); ?>">
                <img src="<?php echo esc_url($img_object['sizes']['medium']); ?>" alt="alt">
            </a>

            <?php
            $count++;
        } ?>

    </div>

<?php } ?>

<button class="btn acf-loadmore" onclick="javascript: acf_repeater_show_more();"
    <?php if ($total_rows < $count) { ?> style="display: none;" <?php } ?>>
    показать ещё
</button>



<!-- AJAX загрузка -->
<script>
    let my_repeater_field_post_id = <?php echo $post->ID; ?>;
    let my_repeater_field_offset = <?php echo $number; ?>;
    let my_repeater_field_nonce = '<?php echo wp_create_nonce('my_repeater_field_nonce'); ?>';
    let my_repeater_ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    let my_repeater_more = true;

    function acf_repeater_show_more() {
        // делаем AJAX запрос 
        jQuery.post(my_repeater_ajax_url, {
            // AJAX, который мы настроили в PHP 
            'action': 'acf_repeater_show_more',
            'post_id': my_repeater_field_post_id,
            'offset': my_repeater_field_offset,
            'nonce': my_repeater_field_nonce
        }, function (json) {
            // добавляем контент в контейнер 
            // этот идентификатор должен соответствовать контейнеру 
            // к которому вы хотите добавить контент 
            jQuery('.ajax-container').append(json['content']);
            // обновим смещение 
            my_repeater_field_offset = json['offset'];
            // проверим, есть ли еще что загрузить 
            if (!json['more']) { // если нет, то скроем кнопку загрузки 
                jQuery('.acf-loadmore').css('display', 'none');
            }
        }, 'json');
    }
</script>








    <?php 

	
	if (have_rows('gallery_r')) { ?>

			<div id="my-repeater-list-id" class="masonry bordered">
				<?php 

					while (have_rows('gallery_r')) {
						the_row();
                        $image = get_sub_field('image_r');
						?>
							<div class="brick"><?php echo wp_get_attachment_image( $image, 'full' );?></div>
						<?php 
					} // end while have rows
				?>
			</div>
            <?php } ?>
