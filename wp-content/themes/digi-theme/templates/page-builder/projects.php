<?php
$section_title = get_sub_field('section_title');
$image_1 = get_sub_field('image_1');
$image_2 = get_sub_field('image_2');
$image_3 = get_sub_field('image_3');
$image_4 = get_sub_field('image_4');
$image_5 = get_sub_field('image_5');
$size = 'full';
$button = get_sub_field('button_label');
$button_url = get_sub_field('button_url');
?>
<section class="projects">
    <h2 class="block_title text-center"><?= $section_title; ?></h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="four-grid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="image-item1">
                                <?php echo wp_get_attachment_image( $image_1, $size ); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="image-item2">
                                <?php echo wp_get_attachment_image( $image_2, $size ); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="image-item3">
                                <?php echo wp_get_attachment_image( $image_3, $size ); ?>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="image-item4">
                                <?php echo wp_get_attachment_image( $image_4, $size ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="vertical-grid">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo wp_get_attachment_image( $image_5, $size ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?= $button_url; ?>" class="button cta-button cta-button-grey"><span>+</span><?= $button; ?></a>
    </div>
</section>