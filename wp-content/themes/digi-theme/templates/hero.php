<?php 
$bg = get_the_post_thumbnail_url();
$title_1 = get_field('line_1');
$padding_1 = get_field('fisrt_spacing_left');
$title_2 = get_field('line_2');
$padding_2 = get_field('second_spacing_left');
$title_3 = get_field('line_3');
$padding_3 = get_field('third_spacing_left');
?>
<?php if($title_1) : ?>
<section class="hero" style="background: url(<?= $bg; ?>) center center / cover no-repeat;">
    <div class="container-fluid">
        <h1 class="h1 general-title"><span class="title-1 hero-title" style="padding-left: <?= $padding_1; ?>%"><?= $title_1; ?></span><span class="title-2 hero-title" style="padding-left: <?= $padding_2; ?>%"><?= $title_2; ?></span><span class="title-3 hero-title" style="padding-left: <?= $padding_3; ?>%"><?= $title_3; ?></span></h1>
        <img src="<?php bloginfo('template_url'); ?>/img/arr_down.svg" alt="Arrow Down" class="arrow_down">
    </div>
</section>
<?php endif; ?>
