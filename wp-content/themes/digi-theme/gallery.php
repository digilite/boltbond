<?php 
/**
* Template Name: Gallery
*
* @package WordPress
* @subpackage Digilite Theme
*/
get_header(); ?>

<div class="container">
    <h1 class="block_title text-center"><?php the_title(); ?></h1>
    <div id="mygallery">
        <?php echo do_shortcode('[justified_image_grid gallery=165]'); ?>
    </div>
</div>
<?php get_template_part("content");
get_footer(); ?>