<?php 
/**
* Template Name: Services
*
* @package WordPress
* @subpackage Digilite Theme
*/
get_header(); 
get_template_part("templates/hero"); ?>

<section class="services-section">
    <div class="services-grid">
        <?php 
        $args = array( 
            'post_type' => 'services',
            'posts_per_page' => 3
        ); 

        $loop = new WP_Query( $args ); 
        $count = 1;
        ?>
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="grid-one-row  flex-container">
            <div class="half-width <?php if($count % 2 === 0): echo "ct-info text-center ct-info-".$count.""; else: echo "ct-image"; endif; ?>" style="<?php if($count % 2 === 0): echo ''; else: echo "background: url(".get_the_post_thumbnail_url( get_the_ID(), 'full').") center center / cover no-repeat;"; endif; ?>">
                <?php if($count % 2 === 0): ?>
                <h4 class="service-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                <?php the_content();
                else: ?>
                
                <?php endif; ?>
            </div>
            <div class="half-width <?php if($count % 2 === 0): echo 'ct-image'; else: echo "ct-info text-center ct-info-".$count.""; endif; ?>" style="<?php if($count % 2 === 0): echo "background: url(".get_the_post_thumbnail_url( get_the_ID(), 'full').") center center / cover no-repeat;"; else: echo ""; endif; ?>">
                <?php if($count % 2 === 0):
                
                else: ?>
                <h4 class="service-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                <?php the_content();
                endif; ?>
            </div>
        </div>
        <?php $count++; ?>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
</section>

<?php get_template_part("content");
get_footer(); ?>