<?php 
    if( have_rows("page_builder") ):

        // loop through the rows of data
        while ( have_rows("page_builder") ) : the_row();
            switch (get_row_layout()) :
                case "image_with_text":
                    get_template_part("templates/page-builder/image-with-text");
                    break;
                case "cta":
                    get_template_part("templates/page-builder/cta");
                    break;
                case "services_carousel":
                    get_template_part("templates/page-builder/carousel");
                    break;
                case "projects":
                    get_template_part("templates/page-builder/projects");
                    break;
                case "gallery":
                    get_template_part("templates/page-builder/gallery");
                    break;
                case "instagram":
                    get_template_part("templates/page-builder/instagram");
                    break;
            endswitch;
        endwhile;
    else:
        the_content();
    endif;
?>