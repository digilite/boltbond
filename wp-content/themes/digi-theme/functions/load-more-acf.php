<?php
/**
 * ACF AJAX подгрузка
 */

// добавляем action для авторизованных пользователей 
add_action('wp_ajax_acf_repeater_show_more', 'acf_repeater_show_more');
// добавляем action для не авторизованных пользователей 
add_action('wp_ajax_nopriv_acf_repeater_show_more', 'acf_repeater_show_more');

function acf_repeater_show_more()
{
    // валидация Nonce («Одноразовые числа») 
    if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'my_repeater_field_nonce')) {
        exit;
    }
    // убедимся, что у нас есть другие значения 
    if (!isset($_POST['post_id']) || !isset($_POST['offset'])) {
        return;
    }
    $show = 3; // по сколько отображать 
    $start = $_POST['offset'];
    $end = $start + $show;
    $post_id = $_POST['post_id'];
    // используем объектный буфер для захвата вывода html (объектные буферы упрощают работу с кодом) 
    // в качестве альтернативы вы можете создать переменную вроде $html 
    // и добавлять содержимое в эту строку 
    ob_start();
    if (have_rows('gallery_r', $post_id)) {
        $total = count(get_field('gallery_r', $post_id));
        $count = 0;

        while (have_rows('gallery_r', $post_id)) {
            the_row();
            if ($count < $start) {
                // продолжаем показывать и увеличивать счётчик 
                $count++;
                continue;
            }
            ?>
            <?php $img_object = get_sub_field('image_r'); ?>

            <div class="grid-item">
                <img src="<?php echo esc_url($img_object['sizes']['medium']); ?>" alt="alt">
            </div>

            <?php
            $count++;
            if ($count == $end) {
                break; # если показали все строки повторителя, выходим из цикла 
            }
        }
    }
    $content = ob_get_clean();
    // проверим, показали ли мы последний элемент 
    $more = false;
    if ($total > $count) {
        $more = true;
    }
    // выводим наши 3 значения в виде массива в кодировке json 
    echo json_encode(array('content' => $content, 'more' => $more, 'offset' => $end));
    exit;
}