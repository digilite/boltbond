<?php 
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage Digilite Theme
*/
get_header();
$title_1 = get_field('contact_us_title_first', 'option');
$title_2 = get_field('contact_us_title_second', 'option');
$title_3 = get_field('contact_us_title_third', 'option');
$phone = get_field('phone_number', 'option');
$email = get_field('email', 'option');
$cu_bg = get_field('contact_us_background', 'option');
?>

<section class="contact" style="background: url(<?= $cu_bg; ?>) center center / cover no-repeat;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="h1 contact-us-title"><span class="title-1"><?= $title_1; ?></span><span class="title-2"><?= $title_2; ?></span><span class="title-3"><?= $title_3; ?></span></h1>
                <div class="contact-us-info">
                    <?php 
                        $clear_phone = str_replace(' ', '', $phone);
                        $part_phone = str_replace(')', '', $clear_phone);
                        $part2_phone = str_replace('(', '', $part_phone);
                        $final_phone = str_replace('-', '', $part2_phone);
                    ?>
                    <a href="mailto:<?= $email; ?>" class="email"><?= $email; ?></a>
					<a href="tel:<?= $final_phone; ?>" class="phone"><?= $phone; ?></a>
                </div>
            </div>
            <div class="col-md-6">
                <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 1,) ); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>